CREATE DEFINER=`root`@`localhost` PROCEDURE `preparar_envio`(
	IN p_id_cliente VARCHAR(10), 
    IN p_id_envio INT)
BEGIN
  DECLARE envios_ready INT;
  DECLARE cursor_id_email VARCHAR(50);
  DECLARE cursor_id_lista INT;
  DECLARE c_usuarios CURSOR FOR
    select u.id_email, u.id_lista from usuarios u, envios_listas el, envios e
	  where el.id_envio = p_id_envio
	    and u.id_lista = el.id_lista
		and e.id_envio = el.id_envio
		and e.id_cliente = p_id_cliente
		and estado = 'X';

	
	update envios set estado = 'X'
	 where id_envio = p_id_envio
	   and id_cliente = p_id_cliente
	   and estado = 'P';
       
	SELECT @envios_ready= ROWCOUNT();    
    
  OPEN c_usuarios;
  
loop	
begin
	FETCH c_usuarios INTO cursor_id_email, cursor_id_lista; 
	  insert into enviados (id_envio, id_email, id_lista, estado)
	    values (p_id_envio, cursor_id_email, cursor_id_lista, 'P');
	
end;
	commit;
end loop;
  CLOSE c_usuarios;
  update envios set estado = 'L'
	where  id_envio = p_id_envio
	and id_cliente = p_id_cliente
	and estado = 'X';
end
