DROP TRIGGER IF EXISTS TRACKING_AIR;
DELIMITER \
CREATE TRIGGER TRACKING_AIR 
  after insert ON TRACKING 
    for each row
BEGIN
  DECLARE v_id_cliente VARCHAR(40);
    IF NEW.tipo='B' THEN    
    set @v_id_cliente=(select id_cliente from envios where id_envio = new.id_envio);
  
      insert into usuarios_borrados (
      id_email, id_lista, email, nombre, apellidos, tratamiento, fecha_creacion,
      fec_ult_actividad, t1, t2, t3, fecha_baja, motivo_baja, ip_baja)
      select id_email, id_lista, email, nombre, apellidos, tratamiento, fecha_creacion,
         fec_ult_actividad, t1, t2, t3, sysdate, 'U', new.ip
         from usuarios where id_email = new.id_email and id_lista = new.id_lista;   
         
      delete from usuarios where id_email = new.id_email and id_lista = new.id_lista;  
      update listas set num_registros = num_registros-1 where id_lista = new.id_lista;  
    END IF;
END;
\
DELIMITER ;