CREATE TRIGGER TRACKING_AIR_AFTER_INSERT
AFTER INSERT ON TRACKING FOR EACH ROW BEGIN
  IF NEW.tipo = 'B' THEN
  
	  INSERT INTO usuarios_borrados (
		id_usuario, id_email, id_lista, email, nombre, apellidos, tratamiento, fecha_creacion,
		fec_ult_actividad, t1, t2, t3, fecha_baja, motivo_baja, ip_baja)
		   SELECT id_usuario, id_email, id_lista, email, nombre, apellidos, tratamiento, fecha_creacion,
		   fec_ult_actividad, t1, t2, t3, CURRENT_DATE(), 'U', NEW.ip
		   FROM usuarios WHERE usuarios.id_email = NEW.id_email AND usuarios.id_lista = NEW.id_lista;
		   
	  DELETE FROM usuarios WHERE id_email = NEW.id_email AND id_lista = NEW.id_lista;
	  
	  UPDATE listas SET num_registros = num_registros-1 WHERE id_lista = NEW.id_lista;
	  
	END IF;
END
