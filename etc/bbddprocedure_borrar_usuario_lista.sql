CREATE DEFINER=`root`@`localhost` PROCEDURE `borrar_usuario_listas`(
  IN p_id_email INT, IN p_id_lista INT, IN p_motivo VARCHAR(40), IN p_ip_baja VARCHAR(10), IN p_id_cliente INT
  )
BEGIN
   DECLARE v_id_cliente VARCHAR(40);
   DECLARE n INT DEFAULT 0;
   DECLARE l INT DEFAULT 0;


  -- Comprobamos si la lista pertenece al cliente
   SELECT @v_id_cliente=id_cliente FROM listas WHERE id_lista = p_id_lista AND id_cliente=p_id_cliente;


    -- Comprobamos si esta en la lista

   SELECT @l=1 FROM usuarios WHERE id_lista = p_id_lista AND id_email = p_id_email;

  -- Si est� insertamos en borrados y la eliminamos

   INSERT INTO usuarios_borrados (
  id_email, id_lista, email, nombre, apellidos, tratamiento, fecha_creacion,
  fec_ult_actividad, t1, t2, t3, fecha_baja, motivo_baja, ip_baja)
     SELECT id_email, id_lista, email, nombre, apellidos, tratamiento, fecha_creacion,
     fec_ult_actividad, t1, t2, t3, sysdate, p_motivo, p_ip_baja
     FROM usuarios WHERE id_email = p_id_email AND id_lista = p_id_lista;

   DELETE FROM usuarios WHERE id_email = p_id_email AND id_lista = p_id_lista;

   UPDATE listas SET num_registros = num_registros-1 WHERE id_lista = p_id_lista;
   COMMIT;
end