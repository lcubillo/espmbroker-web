CREATE DEFINER=`root`@`localhost` PROCEDURE `borrar_usuario`(  
  IN p_id_email INT,   
  IN p_id_lista INT,     
    IN p_motivo VARCHAR(40),   
    IN p_ip_baja VARCHAR(10),   
    OUT deleted_users INT )
BEGIN
  DECLARE list_founded INT DEFAULT 0; 
  DECLARE cursor_id_list INT;
  DECLARE v_id_cliente VARCHAR(40);
  DECLARE ccc CURSOR FOR SELECT id FROM listas WHERE id_cliente = v_id_cliente;
  DECLARE EXIT HANDLER FOR NOT FOUND
  BEGIN
  SET deleted_users = 0;
  END;
  
  
  
  OPEN ccc;
  -- Bucle de listas
  LOOP
  FETCH ccc INTO cursor_id_list;
    -- Comprobamos si esta en la lista
     
  
  SELECT 1 INTO list_founded FROM usuarios WHERE id_lista = cursor_id_list AND id_email = p_id_email;
  
  -- Si est� insertamos en borrados y la eliminamos
  IF list_founded=1 THEN 
    INSERT INTO usuarios_borrados (
    id_email, id_lista, email, nombre, apellidos, tratamiento, fecha_creacion, 
    fec_ult_actividad, t1, t2, t3, fecha_baja, motivo_baja, ip_baja) 
      SELECT id_email, id_lista, email, nombre, apellidos, tratamiento, fecha_creacion, 
      fec_ult_actividad, t1, t2, t3, sysdate, p_motivo, p_ip_baja
      FROM usuarios WHERE id_email = p_id_email AND id_lista = c.id_lista;
    
    DELETE FROM usuarios WHERE id_email = p_id_email AND id_lista = c.id_lista;
    COMMIT;
    SET deleted_users = deleted_users + 1;
      
  END IF;
  END LOOP; 
  CLOSE ccc;
  
END